{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat enkele algemene polymorfe functies.
-}

module Util (zipWithL, zipWithR, comb, fst3, uncurry3) where

import Control.Applicative (liftA2)

-- | Version of ZipWith guaranteed to produce a list of the same length as the second input.
zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR _ _      []     = []
zipWithR _ []     bs     = bs
zipWithR f (a:as) (b:bs) = f a b : zipWithR f as bs

-- | Deze functie is een variatie op 'ZipWith'. Hier produceren we een lijst die gegarandeerd dezelfde lengte is als de lijst op de eerste input. Als er voor sommige elementen (a) geen elementen (b) op de index van de andere lijst staan, schrijf je gewoon de elementen van a op op die index.
zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL _ []      _     = []
zipWithL _ bs     []     = bs
zipWithL f (a:as) (b:bs) = f a b : zipWithL f as bs


-- | Use a given binary operator to combine the results of applying two separate functions to the same value. Alias of liftA2 specialised for the function applicative.
comb :: (b -> b -> b) -> (a -> b) -> (a -> b) -> a -> b
comb = liftA2

-- | Hier pakken we simpelweg het eerste element van een tuple, de oplossing spreekt voor zichzelf.
fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a

-- | Met deze funtie geef je een bepaalde functie mee en een tuple met drie elementen en hier zullen we die functie op de elementen uit die tuple toepassen. 
uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a,b,c) = f a b c
