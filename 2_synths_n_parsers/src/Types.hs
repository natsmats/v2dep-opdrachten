{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat alle type-declaraties, instanties en type-gerelateerde functies, zodat we deze makkelijk in meerdere modules kunnen importeren.
-}

{-# LANGUAGE TypeApplications #-}

module Types ( Beats, Hz, Samples, Seconds, Semitones, Track, Ringtone
             , Tone(..), Octave(..), Duration(..), Note(..)
             , Sound, floatSound, intSound, (<+>), asFloatSound, asIntSound, getAsFloats, getAsInts
             , Instrument, instrument, Modifier, modifier, modifyInstrument, arrange
             ) where

import Data.Int (Int32)
import Util

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

data Sound = IntFrames [Int32] | FloatFrames [Float]
  deriving (Eq, Show)

floatSound :: [Float] -> Sound
floatSound = FloatFrames

intSound :: [Int32] -> Sound
intSound = IntFrames
{-
TODO Maak instances voor `Sound` voor `Semigroup` en `Monoid`. De monoid-operatie staat in dit geval voor het sequentieel (achter elkaar) combineren van 
audiofragmenten. Het is van belang dat `IntFrames` alleen met `IntFrames` worden gecombineerd, en dito voor `FloatFrames`. Bij twee verschillende gevallen moet je 
beiden naar hetzelfde formaat converteren, idealiter `FloatFrames`. Wat is een leeg audiofragment in deze context?
-}

instance Semigroup Sound where

instance Monoid Sound where

-- | In deze funtie voegen we twee geluiden bij elkaar toe. Maar als één geluid langer doorgaat dan de ander, gaat dat geluid alsnog door. 
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y) = if length x < length y  -- | Als het geluid van de tweede input langer doorgaat dan het geluid van de eerste.
                                      then FloatFrames $ zipWithR (+) x y  -- | Dan gebruiken we zipWithR. Deze functie hadden we in 'Util.hs' gemaakt. Dit zorgt ervoor dat de twee geluiden over elkaar heen gaan, maar dat het langere geluid gegarandeerd nog doorgaat nadat het eerste geluid klaar is.
                                      else FloatFrames $ zipWithL (+) x y  -- | Dan gebruiken we zipWithL. Deze functie hadden we in 'Util.hs' gemaakt. Dit zorgt ervoor dat de twee geluiden over elkaar heen gaan, maar dat het langere geluid gegarandeerd nog doorgaat nadat het eerste geluid klaar is. En als de geluiden even lang zijn zullen ze tegelijkertijd stoppen.
x <+> y = asFloatSound x <+> asFloatSound y

asFloatSound :: Sound -> Sound
asFloatSound (IntFrames fs) = floatSound $ map ( (/ fromIntegral (div (maxBound @Int32 ) 2 )) . fromIntegral ) fs
asFloatSound fframe = fframe

-- | Hier doen we hetzelfde als bij 'asFloatSound' alleen zetten we het getal over naar een Integer.
asIntSound :: Sound -> Sound
asIntSound (FloatFrames fs) = intSound $ map (\n -> round $ fromIntegral (div (maxBound @Int32) 2) * n) fs
asIntSound fframe = fframe

getAsFloats :: Sound -> [Float]
getAsFloats sound = case asFloatSound sound of
  (FloatFrames ffs) -> ffs
  _ -> error "asFloatSound did not return FloatFrames"

getAsInts :: Sound -> [Int32]
getAsInts sound = case asIntSound sound of
  (IntFrames ifs) -> ifs
  _ -> error "asIntSound did not return IntFrames"

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- | Hier krijgen we een Instrument en een Modifier binnen. We passen het Instrument op de Modifier toe en maken er dan weer een Instrument van.

modifyInstrument :: Instrument -> Modifier -> Instrument
modifyInstrument (Instrument x) (Modifier f) = Instrument (\hz s -> f $ x hz s)

-- | Hier krijgen we een Instrument, het aantal hertz en het aantal seconden mee. Het aantal hertz en het aantal seconden passen we toe op het Instrument en wat daar uitkomt zetten we weer om in 'Sound'
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument i) hz s = floatSound $ i hz s
