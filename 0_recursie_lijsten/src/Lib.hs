module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- |berekent de som van een lijst met getallen. Er komt een lijst met getallen binnen en één normaal getal uit.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + ex1 (xs)

-- |verhoogt alle getallen in een lijst met 1. Er komt lijst met getallen in en er komt een lijst van getallen eruit
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = [x + 1] ++ ex2(xs)

-- |vermenigvuldigt elk getal uit de lijst met -1. Er komt een lijst met getallen in en er komt een lijst van getallen eruit
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = [(-x)] ++ ex3(xs)

-- |plakt twee lijsten aan elkaar. Er komen twee lijsten met getallen in en er komt een lijst van getallen eruit
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] [x] = []
ex4 a (x:xs) = a ++ ex4(xs)([x])

-- |telt twee lijsten paarsgewijs bij elkaar op. Er komt een lijst met getallen in en er komt een lijst van getallen eruit
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) (x2:xs2) = [x + x2] ++ ex5 (xs)(xs2)

-- |in deze functie vermenigvuldigen we twee lijsten paarsgewijs. Er komt een lijst met getallen in en er komt een lijst van getallen eruit
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) (x2:xs2) = [(x * x2)] ++ ex6 (xs)(xs2)

<<<<<<< HEAD
-- |in deze functie tellen we de getallen van twee lijsten paargewijs bij elkaar op en dan nemen we de som van al deze getallen. Er komt een lijst met getallen in en er komt een getal uit
=======
-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
>>>>>>> 15f6e502c952be37312eddb3823ebe24ca7bc074
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 (x:xs) (x2:xs2) = (x * x2) + ex7 (xs)(xs2)
