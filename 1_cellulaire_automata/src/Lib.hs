{-|
    Module      : Lib
    Description : Tweede checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata [<https://mathworld.wolfram.com/Rule30.html>].
-}

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. Dit type representeert een 1-dimensionale lijst, met een
-- enkel element dat "in focus" is. Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.

-- * FocusList

{- | The focussed list [0 1 2 ⟨3⟩ 4 5] is represented as @FocusList [3,4,5] [2,1,0]@. The first element (head) of the first list is focussed, and is easily and cheaply accessible.
 -   The items before the focus are placed in the backwards list in reverse order, so that we can easily move the focus by removing the focus-element from one list and prepending
 -   it to the other.
-}
data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving (Show,Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

-- | Deze functie zet een FocusLijst om in een normale lijst op de goede volgorde.
toList :: FocusList a -> [a]
toList (FocusList x y) = reverse(y) ++ x

-- | Deze functie zet een normale lijst om in een FocusLijst. De focus zal op het eerste element staan.
fromList :: [a] -> FocusList a
fromList x = FocusList {forward = x, backward = []}

-- | Move the focus one to the left
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- | Verplaats de focus in een FocusLijst één naar rechts.

goRight :: FocusList a -> FocusList a
goRight (FocusList (x:xs) y) = FocusList xs (x:y)

-- | Deze functie verplaatst de focus geheel naar links (het eerste element).
leftMost :: FocusList a -> FocusList a
leftMost x = fromList $ toList x  -- We maken er eerst een normale lijst van en vervolgens weer een FocusList. Hier is de focus altijd helemaal links.

-- Dit is een functie die de focus in een FocusList geheel naar rechts opschuift.
rightMost :: FocusList a -> FocusList a
rightMost (FocusList [] b) = goLeft(FocusList [] b)  -- Als elk element in de backward staat, verplaatsen we de focus één keer naar links zodat de focus staat op het laatste element.
rightMost (FocusList fw bw) = rightMost(goRight(FocusList fw bw))

-- | De functies totalLeft en totalRight doen hetzelfde als 'goLeft' en 'goRight. Het enige verschil is dat ze op lege waardes kunnen focussen wanneer ze buiten de indexen van de lijst komen.

totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft (FocusList x []) = FocusList (mempty:x) []  -- | Voegt een 'dode' waarde links toe, en focust zich daarna hierop.
totalLeft (FocusList x y) = goLeft(FocusList x y)

totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight (FocusList x y) = if length x == 1
                             then FocusList{forward = mempty, backward = y ++ x}  -- | Voegt een 'dode' waarde rechts toe, en focust zich daarna hierop.
                             else goRight(FocusList x y)


-- | In deze functie passen we een fuctie toe op zowel de forward als de backward lijst van een FocusLijst.
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList f (FocusList x y) = FocusList (map f x) (map f y)

-- | In deze functie passen we een zipWith toe op de forward en op de backward lijsten van twee FocusLijsten toe. Hieruit krijgen we weer één FocusLijst.

zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
zipFocusListWith f (FocusList x y) (FocusList x2 y2) = FocusList (zipWith f x x2) (zipWith f y y2)

-- | In deze functie folden we een gehele FocusLijst. 

foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList f (FocusList x y) = f (foldr1 f (reverse y)) (foldl1 f x)

-- | Test function for the behaviour of foldFocusList.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld     == 15
               , foldFocusList (-) intVoorbeeld     == 7
               , foldFocusList (++) stringVoorbeeld == "012345"
               ]


-- | A cell can be either on or off, dead or alive. What basic type could we have used instead? Why would we choose to roll our own equivalent datatype?
data Cell = Alive | Dead deriving (Show, Eq)

instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- | The state of our cellular automaton is represented as a FocusList of Cells.
type Automaton = FocusList Cell

-- | Start state, per default, is a single live cell.
start :: Automaton
start = FocusList [Alive] []

-- | Alternative start state with 5 alive cells, for shrinking rules.
fiveAlive :: Automaton
fiveAlive = fromList $ replicate 5 Alive

-- | A rule [<https://mathworld.wolfram.com/Rule30.html>] is a mapping from each possible combination of three adjacent cells to the associated "next state".
type Context = [Cell]
type Rule = Context -> Cell

-- | Deze functie werkt hetzelfde als 'head'. Het pakt het eerste element uit een lijst. Het enige verschil is, dat wanneer er niks in een lijst zit, je een basiswaarde terugkrijgt.
safeHead :: a        -- ^ Default value
         -> [a]      -- ^ Source list
         -> a
safeHead a [] = a
safeHead a (x:xs) = x  


-- Deze functie werkt hetzelfde als `take`, maar met een extra argument. Als de lijst lang genoeg is dan werkt de functie hetzelfde als `take` en worden de eerste `n` elementen teruggegeven.
-- Als dat niet zo is dan worden zoveel mogelijk elementen teruggegeven, en wordt de lijst daarna tot de gevraagde lengte aangevuld met een meegegeven default-waarde.
takeAtLeast :: Int   -- ^ Number of items to take
            -> a     -- ^ Default value added to the right as padding
            -> [a]   -- ^ Source list
            -> [a]
takeAtLeast n x xs = take n xs ++ replicate (n - length xs) x  -- Als ik niet genoeg waardes heb in een lijst, maak ik een lijst met het aantal missende waardes  aan en dan voeg ik die twee samen. Zo heb ik toch nog genoeg.

-- | In deze functie pakken we door middel van 'takeAtLeast' het focuspunt en de twee waardes aan beide ernaast. Als daar geen waarde staat, wordt er een 'Dead' waarde teruggegeven.
context :: Automaton -> Context
context (FocusList x y) = takeAtLeast 1 Dead y ++ takeAtLeast 2 Dead x

-- | In deze fuctie voegen we een dode waarde aan beide kanten van een automaton toe. 
expand :: Automaton -> Automaton
expand (FocusList x y) = FocusList{forward = x ++ [mempty], backward = y ++ [mempty]} 

-- | A sequence of Automaton-states over time is called a TimeSeries.
type TimeSeries = [Automaton]

-- | Iterate a given rule @n@ times, given a start state. The result will be a sequence of states from start to @n@.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s]
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s)
  where applyRule :: Automaton -> Context 
        applyRule (FocusList [] bw) = []  -- | Als er niks zit in de automaton, geeft ie een lege lijst terug.
        applyRule z = r (context z) : applyRule (goRight z)  -- | Kijkt bij elk element van de automaton of het element dood of levend is.

-- | Convert a time-series of Automaton-states to a printable string.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"

-- | Als de lijst begint met Dead, wordt het Alive.
-- | Dit is alleen niet het geval wanneer de lijst alleen maar bestaat uit Dead.
-- | De rest van de waardes wordes Dead
-- | Op het geval [Alive, Dead, Dead] na. Deze wordt Alive.

rule30 :: Rule
rule30 [Dead, Dead, Dead] = Dead
rule30 [Dead, _, _] = Alive
rule30 [Alive, Dead, Dead] = Alive
rule30 x = Dead
-- ...

-- Je kan je rule-30 functie in GHCi testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}

-- * Rule Generation

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. Zoals op de genoemde pagina te zien is heeft het nummer te maken met binaire
-- codering. De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). Er zijn 8 mogelijke combinaties
-- van 3 van dit soort cellen. Afhankelijke van het nummer dat een regel heeft mapt iedere combinatie naar een levende of dode cel.

-- TODO Definieer allereerst een constante `inputs` die alle 8 mogelijke contexts weergeeft: [Alive,Alive,Alive], [Alive,Alive,Dead], etc.
-- Je mag dit met de hand uitschrijven, maar voor extra punten kun je ook een lijst-comprehensie of andere slimme functie verzinnen.

inputs :: [Context]
inputs = [[Alive,Alive,Alive], [Alive,Alive,Dead], [Alive,Dead,Alive], [Alive,Dead,Dead], [Dead,Alive,Alive], [Dead,Alive,Dead], [Dead,Dead,Alive], [Dead,Dead,Dead]]

-- | If the given predicate applies to the given value, return Just the given value; in all other cases, return Nothing.
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing

-- | Deze functie krijgt een Int binnen en vertaalt dit naar een binary representatie. De 1'en zullen True worden en de 0'en False. Deze staan in een lijst die je terugkrijgt.

binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)


-- | In deze functie krijgen we een normale lijst en een lijst met booleans mee. We zoeken de indexen waar een True staat in de boolean lijst, en geven de waardes die in de andere lijst staan op die indexen terug in een nieuwe lijst.
mask :: [Bool] -> [a] -> [a]
mask xs ys = catMaybes $ zipWith (\b x -> if b then Just x else Nothing) xs ys 

-- TODO Combineer `mask` en `binary` met de library functie `elem` en de eerder geschreven `inputs` tot een rule functie. Denk eraan dat het type Rule een short-hand is voor een
-- functie-type, dus dat je met 2 argumenten te maken hebt. De Int staat hierbij voor het nummer van de regel, dat je eerst naar binair moet omrekenen; de Context `input` is
-- waarnaar je kijkt om te zien of het resultaat met de gevraagde regel Dead or Alive is. Definieer met `where` subset van `inputs` die tot een levende danwel dode cel leiden.
-- Vergeet niet je functie te documenteren.
rule :: Int -> Rule
rule n input = undefined


{- Je kan je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   putStrLn . showPyramid . iterateRule (rule 128) 10 $ fiveAlive

               ▓▓▓▓▓
              ░░▓▓▓░░
             ░░░░▓░░░░
            ░░░░░░░░░░░
           ░░░░░░░░░░░░░
          ░░░░░░░░░░░░░░░
         ░░░░░░░░░░░░░░░░░
        ░░░░░░░░░░░░░░░░░░░
       ░░░░░░░░░░░░░░░░░░░░░
      ░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░

   Als het goed is zal `stack run` nu ook werken met de voorgeschreven main functie; experimenteer met verschillende parameters en zie of dit werkt.
-}


